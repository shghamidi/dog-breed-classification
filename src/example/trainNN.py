import os
from enum import Enum

from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import ModelCheckpoint

from classify.neuralNetwork import neuralNetwork
from data.prepareData import splitData
from data.dataProvider import dataProvider

class dataType(Enum):
    training = 1
    validation = 2
    test = 3

# To store all the configurations required for NN training
# Can be overwritten by an external configuration file (Not implemented)
class config(object):
    # NN structure config
    imgRows, imgCols = 128, 128
    randomCropping = 1 #if 1, no random cropping will be performed. Still working on random cropping
    filtersNum = 48
    poolNum = 2
    #convNum = 7
    fullNetUnitNum = 512
    classNum = 120

    # NN training config
    learningRate = .01    
    learningRateDecay = 1e-6
    momentum = 0.9
    epochNum = 100
    batchSize = 128 
    l2Regularizer=1e-3
    
    # Data configs
    trainingPercentage = 0.6
    valPercentage = 0.2    
    dataDirPath = "./dataset/"
    modelToSave = "./model/"

def main():
        
    print("Preparing data. This might take some time ...")
    splitedBreedsList = splitData(config.dataDirPath, config.trainingPercentage, config.valPercentage)
    
    trainingDataProvider = dataProvider(config, splitedBreedsList, dataType.training)
    valDataProvider = dataProvider(config, splitedBreedsList, dataType.validation)
    testDataProvider = dataProvider(config, splitedBreedsList, dataType.test)
    
    trainingDataGenerator = trainingDataProvider.batchGenerator(config)    
    valDataGenerator = valDataProvider.batchGenerator(config)
    testDataGenerator = testDataProvider.batchGenerator(config)

    print("Building network ...")
    net = neuralNetwork(config)
    net.model.summary()
    
    # Save the model weights after each epoch
    os.makedirs(os.path.join(config.modelToSave), exist_ok=True)
    filepath=os.path.join(config.modelToSave, "weights.{epoch:02d}.hdf5")
    checkpointer = ModelCheckpoint(filepath=filepath, monitor='val_loss', verbose=0, save_best_only=False, mode='auto')
    
    print("Training network ...")
    
    net.model.fit_generator(trainingDataGenerator, samples_per_epoch=trainingDataProvider.totalDataNum, 
                            nb_epoch=config.epochNum, callbacks=[checkpointer], 
                            validation_data=valDataGenerator, nb_val_samples=valDataProvider.totalDataNum)
    
    print("Evaluating network ...")
    score = net.model.evaluate_generator(testDataGenerator, val_samples=testDataProvider.totalDataNum)
    print("eval_loss: %0.4f - eval_acc: %0.4f" %(score[0], score[1])) 

  
if __name__ == '__main__':
    main()
