from keras import optimizers
from keras.models import Sequential
from keras.layers.core import Dense, Flatten, Dropout
from keras.layers.convolutional import Convolution2D, MaxPooling2D
from keras.regularizers import l2

import numpy as np

np.random.seed(999)  # for reproducibility

class neuralNetwork:
    
    def __init__(self, config):
        
        model = Sequential() 
        
        model.add(Convolution2D(config.filtersNum, 7, 7, subsample=(3, 3), 
                                activation='relu', border_mode='same', 
                                input_shape=(3, config.imgRows, config.imgCols)))
        model.add(MaxPooling2D(pool_size=(config.poolNum, config.poolNum)))
        
        
        model.add(Convolution2D(config.filtersNum*2, 5, 5, subsample=(2, 2),
                                activation='relu', border_mode='same'))        
        model.add(MaxPooling2D(pool_size=(config.poolNum, config.poolNum)))
        
        
        model.add(Convolution2D(config.filtersNum*4, 3, 3, subsample=(1, 1), 
                                activation='relu', border_mode='same'))
        model.add(Convolution2D(config.filtersNum*4, 3, 3, subsample=(1, 1), 
                                activation='relu', border_mode='same'))
        model.add(MaxPooling2D(pool_size=(config.poolNum, config.poolNum)))
                
               
        model.add(Flatten())
        model.add(Dense(config.fullNetUnitNum, activation='relu', W_regularizer=l2(config.l2Regularizer)))
        model.add(Dropout(0.5))
        model.add(Dense(config.classNum, activation='softmax'))
        
        
        
        sgd = optimizers.SGD(lr=config.learningRate, decay=config.learningRateDecay, 
                             momentum=config.momentum, nesterov=True)
        model.compile(loss='categorical_crossentropy',
                      optimizer=sgd,
                      metrics=['accuracy'])
        
        self.model = model
