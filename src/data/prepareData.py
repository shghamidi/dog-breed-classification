import os
from urllib import request
import tarfile

def checkData(dataDirPath):
    originalSource = "http://vision.stanford.edu/aditya86/ImageNetDogs/images.tar"
          
    # Check/Download the dataset        
    if not os.path.isdir(os.path.join(dataDirPath, "Images")):
        os.makedirs(dataDirPath, exist_ok=True)
        print('Downloading data from %s' % originalSource)
        request.urlretrieve(originalSource, os.path.join(dataDirPath, "images.tar"))
        print('Extracting data into %s' %dataDirPath)
        tar = tarfile.open(os.path.join(dataDirPath, "images.tar"))
        tar.extractall(dataDirPath)
        tar.close()
        
    dataDirPath = os.path.join(dataDirPath, "Images")
    
    return dataDirPath    


def splitData(dataDirPath, trainingPercentage, valPercentage):
    
    dataDirPath = checkData(dataDirPath)
    breedsList = os.listdir(dataDirPath)
        
    splitedBreedsList = [] # This list will hold all the data after being split
    for breed in breedsList:
        dogList = os.listdir(os.path.join(dataDirPath, breed))
        dogNum = len(dogList)
        trainNum = int(trainingPercentage*dogNum)
        valNum = int(valPercentage*dogNum)
        splitedBreedsList.append([breed, dogList[:trainNum], dogList[trainNum: trainNum + valNum], dogList[trainNum + valNum:]])        
        
    
    return splitedBreedsList
