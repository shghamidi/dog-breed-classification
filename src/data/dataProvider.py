import numpy as np
import os
from PIL import Image
import h5py
#import matplotlib.pyplot as plt

class dataProvider:
    def __init__(self, config, splitedBreedsList, dataType):
        self.dataType = dataType
        self.totalDataNum = config.randomCropping * sum([len(splitedBreedsList[i][dataType.value]) for i in range(len(splitedBreedsList))])
        self.preProcessData(config, splitedBreedsList)
           

    def preProcessData(self,config, splitedBreedsList):       
        h5f = h5py.File(os.path.join(config.dataDirPath, self.dataType.name+".hdf5"), "w")
        dsetFeature = h5f.create_dataset('feature', (self.totalDataNum, 3, config.imgRows, config.imgCols), dtype=np.float32)
        dsetLabel = h5f.create_dataset('label', (self.totalDataNum, ), dtype=np.int32)
        
        # To randomize data while saving        
        idxList = np.arange(self.totalDataNum, dtype="int32")
        np.random.shuffle(idxList)
        
        cnt = 0
        for index, dogList in enumerate(splitedBreedsList):                    
            for dogs in dogList[self.dataType.value]:                
                img = Image.open(os.path.join(config.dataDirPath, "Images", dogList[0], dogs))
                
                if config.randomCropping > 1:
                
                    # Resize the image by preserving its aspect ratio
                    preCroppingSizeRatio = 1.2
                    if img.size[0] > img.size[1]:
                        newCols = int(config.imgCols*preCroppingSizeRatio)
                        newRows = int(img.size[0]*config.imgCols*preCroppingSizeRatio / float(img.size[1]))                       
                    else:
                        newRows  = int(config.imgRows*preCroppingSizeRatio)
                        newCols = int(img.size[1]*config.imgRows*preCroppingSizeRatio / float(img.size[0])) 
                                         
                    img = img.resize((newRows, newCols), Image.ANTIALIAS)
                    #img.show()
                    
                    # Do random cropping
                    remainedRows = newRows - config.imgRows
                    remainedCols = newCols - config.imgCols
                    
                    randomCorners = (np.random.rand(config.randomCropping, 2)*[remainedRows, remainedCols]).astype(int)
                    for crp in range(config.randomCropping):
                        imgCropped = img.crop((randomCorners[crp, 0], randomCorners[crp, 1], 
                                         randomCorners[crp, 0]+config.imgRows, randomCorners[crp, 1]+config.imgCols))
                
                        imgArray = np.asarray(imgCropped)
                        imgArray = np.transpose(imgArray, axes = [2,0,1]) / 255
                        imgArray = imgArray.astype(np.float32) 
                         
                        dsetFeature[idxList[cnt]] = imgArray[0:3]
                        dsetLabel[idxList[cnt]] = index        
                        cnt += 1
                else:
                    img = img.resize((config.imgRows, config.imgCols), Image.ANTIALIAS)            
                    imgArray = np.asarray(img)
                    imgArray = np.transpose(imgArray, axes = [2,0,1]) / 255
                    imgArray = imgArray.astype(np.float32) 
                     
                    dsetFeature[idxList[cnt]] = imgArray[0:3]
                    dsetLabel[idxList[cnt]] = index        
                    cnt += 1

                #img.show()            
                #plt.figure()
                #plt.imshow(imgResizedArr[0,:,:], cmap='Greys_r')
        h5f.close()
 
    def batchGenerator(self, config):
        while(1):
            h5f = h5py.File(os.path.join(config.dataDirPath, self.dataType.name+".hdf5"), "r")
            batchNum = int(np.ceil(self.totalDataNum/float(config.batchSize)))
            
            for i in range(batchNum):
                feature = h5f['feature'][i*config.batchSize: (i+1)*config.batchSize]
                label = h5f['label'][i*config.batchSize: (i+1)*config.batchSize]
                label = np.eye(config.classNum, config.classNum)[label]

                # shuffle again!
                idxList = np.arange(feature.shape[0], dtype="int32")
                np.random.shuffle(idxList)
                
                feature = feature[idxList]
                label = label[idxList]
                
                yield feature, label
                                
            h5f.close()
